/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mymmodpoww.oxapplication;

import java.util.Scanner;

/**
 *
 * @author Acer
 */
public class OXapplication {

    static String[][] board;
    static Scanner input;

    public static void main(String[] args) {
        input = new Scanner(System.in);
        board = new String[3][3];
        boolean win = false;
        int xo = 9;
        int count = 0;
        int count1 = 0;
        board[0][0] = "-";
        board[0][1] = "-";
        board[0][2] = "-";
        board[1][0] = "-";
        board[1][1] = "-";
        board[1][2] = "-";
        board[2][0] = "-";
        board[2][1] = "-";
        board[2][2] = "-";

        
        System.out.println("Welcome to OX Game");
        System.out.println("  1 2 3");

        for (int i = 0; i < 3; i++) {
            System.out.print(i + 1 + " ");
            for (int j = 0; j < 3; j++) {
                System.out.print(board[i][j] + " ");
            }
            System.out.println("");
        }
        while (!win == true) {
            if (xo % 2 != 0) {
                System.out.println("X Turn");
                System.out.println("Please input row col : ");
                int row = input.nextInt();
                int col = input.nextInt();
                board[row - 1][col - 1] = "X";
                System.out.println("  1 2 3");
                for (int i = 0; i < 3; i++) {
                    System.out.print(i + 1 + " ");
                    for (int j = 0; j < 3; j++) {
                        System.out.print(board[i][j] + " ");
                    }
                    System.out.println("");
                }
                xo--;
                count++;
                if ((board[0][0] == "X") && (board[0][1] == "X") && (board[0][2] == "X")
                        || (board[1][0] == "X") && (board[1][1] == "X") && (board[1][2] == "X")
                        || (board[2][0] == "X") && (board[2][1] == "X") && (board[2][2] == "X")
                        || (board[0][0] == "X") && (board[1][0] == "X") && (board[2][0] == "X")
                        || (board[0][1] == "X") && (board[1][1] == "X") && (board[2][1] == "X")
                        || (board[0][2] == "X") && (board[1][2] == "X") && (board[2][2] == "X")
                        || (board[0][0] == "X") && (board[1][1] == "X") && (board[2][2] == "X")
                        || (board[0][2] == "X") && (board[1][1] == "X") && (board[2][0] == "X")) {
                    System.out.println("Player X Win...");
                    System.out.println("Bye Bye...");
                    win = true;
                }
                if (count >= 5) {
                    break;
                }
            } else if (xo % 2 == 0) {
                System.out.println("O Turn");
                System.out.println("Please input row col");
                int rowo = input.nextInt();
                int colo = input.nextInt();
                board[rowo - 1][colo - 1] = "O";
                System.out.println("  1 2 3");
                for (int i = 0; i < 3; i++) {
                    System.out.print(i + 1 + " ");
                    for (int j = 0; j < 3; j++) {
                        System.out.print(board[i][j] + " ");
                    }
                    System.out.println("");
                }
                xo--;
                count1++;
                if ((board[0][0] == "O") && (board[0][1] == "O") && (board[0][2] == "O")
                        || (board[1][0] == "O") && (board[1][1] == "O") && (board[1][2] == "O")
                        || (board[2][0] == "O") && (board[2][1] == "O") && (board[2][2] == "O")
                        || (board[0][0] == "O") && (board[1][0] == "O") && (board[2][0] == "O")
                        || (board[0][1] == "O") && (board[1][1] == "O") && (board[2][1] == "O")
                        || (board[0][2] == "O") && (board[1][2] == "O") && (board[2][2] == "O")
                        || (board[0][0] == "O") && (board[1][1] == "O") && (board[2][2] == "O")
                        || (board[0][2] == "O") && (board[1][1] == "O") && (board[2][0] == "O")) {
                    
                    System.out.println("Player O Win...");
                    System.out.println("Bye Bye...");
                    win = true;
                }
                if (count1 >= 5) {
                    break;
                }

            }

        }
        if (count >= 5 && xo == 0) {
            System.out.println("Draw");
            System.out.println("Bye Bye...");
        }

    }
}